package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

import java.util.Objects;

public class Smile extends Figure {

    private double base;

    public Smile(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_SMILE, cX, cY, lineWidth, color);
    }

    public Smile(double cX, double cY, double lineWidth, Color color, double base) {
        this(cX, cY, lineWidth, color);
        this.base = base < 21 ? 21 : base;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Smile smile = (Smile) o;
        return Double.compare(smile.base, base) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(base);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Arc{");
        sb.append("base=").append(base);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.beginPath();
        gc.strokeOval(cX - base, cY - base, base * 2, base * 2);
        gc.strokeArc(cX - 2 * base / 3, cY - 2 * base / 3, 2 * base - 2 * base / 3, 2 * base - 2 * base / 3, 180, 180, ArcType.OPEN);
        gc.strokeOval(cX - base / 3, cY - base / 2, base / 5, base / 5);
        gc.strokeOval(cX + base / 3, cY - base / 2, base / 5, base / 5);
        gc.setFill(Color.RED);
        gc.fillArc(cX - base / 2, cY + base / 2, base / 3, base / 3, 130, 230, ArcType.OPEN);
    }

}
