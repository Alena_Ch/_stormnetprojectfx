package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

import java.util.Objects;

public class Car extends Figure {

    private double base;
    private double pointX1;
    private double pointY1;


    public Car(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_CAR, cX, cY, lineWidth, color);
    }

    public Car(double cX, double cY, double lineWidth, Color color, double base, double pointX1, double pointY1) {
        this(cX, cY, lineWidth, color);
        this.base = base < 10 ? 10 : base;
        this.pointX1 = pointX1;
        this.pointY1 = pointY1;

    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getPointX1() {
        return pointX1;
    }

    public void setPointX1(double pointX1) {
        this.pointX1 = pointX1;
    }

    public double getPointY1() {
        return pointY1;
    }

    public void setPointY1(double pointY1) {
        this.pointY1 = pointY1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Double.compare(car.base, base) == 0 &&
                Double.compare(car.pointX1, pointX1) == 0 &&
                Double.compare(car.pointY1, pointY1) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(base, pointX1, pointY1);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Smth{");
        sb.append("base=").append(base);
        sb.append(", pointX1=").append(pointX1);
        sb.append(", pointY1=").append(pointY1);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.beginPath();
        gc.setFill(Color.WHITE);
        gc.fillArc(cX, cY, 2.5 * base, 2.5 * base, 180, -180, ArcType.CHORD);
        gc.setFill(Color.SILVER);
        gc.fillArc(cX + base / 4, cY + base / 5, 1.5 * base, 1.5 * base, 180, -90, ArcType.ROUND);
        gc.fillArc(cX + base / 4, cY + base / 5, 2 * base, 1.5 * base, 90, -90, ArcType.ROUND);

        double[] xCoord = {cX, cX + 2.5 * base, cX + 2.5 * base, cX};
        double[] yCoord = {cY + 1.21 * base, cY + 1.21 * base, cY + 2 * base, cY + 2 * base};
        gc.setFill(Color.BLUE);
        gc.fillPolygon(xCoord, yCoord, 4);
        gc.setFill(Color.BLUE);
        gc.fillArc(cX - base, cY + 1.21 * base, 2 * base, 1.58 * base, 180, -180, ArcType.CHORD);
        gc.fillArc(cX + 1.5 * base, cY + 1.21 * base, 2 * base, 1.58 * base, 180, -180, ArcType.CHORD);
        gc.setFill(Color.BLACK);
        gc.fillOval(cX - base / 3, cY + 1.5 * base, base, base);
        gc.fillOval(cX + 2 * base, cY + 1.5 * base, base, base);
        gc.setFill(Color.YELLOW);

    }
}
