package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Triangle extends Figure{
    private double bottom;

    public Triangle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRIANGLE, cX, cY, lineWidth, color);
    }

    public Triangle(double cX, double cY, double lineWidth, Color color, double bottom) {
        this(cX, cY, lineWidth, color);
        this.bottom = bottom < 10 ? 10 : bottom;
    }

    public double getBottom() {
        return bottom;
    }

    public void setBottom(double bottom) {
        this.bottom = bottom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.bottom, bottom) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(bottom);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Triangle{");
        sb.append("bottom=").append(bottom);
        sb.append(", cX=").append(cX);
        sb.append(", cY=").append(cY);
        sb.append(", lineWidth=").append(lineWidth);
        sb.append(", color=").append(color);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolygon(new double[]{cX, cX+bottom/2, cX-bottom/2}, new double[]{cY-bottom/2, cY+bottom/2, cY+bottom/2}, 3);

    }

}
