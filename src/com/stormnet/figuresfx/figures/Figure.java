package com.stormnet.figuresfx.figures;

import com.stormnet.drawutils.Drawable;
import javafx.scene.paint.Color;

public abstract class Figure implements Drawable {
    public static final int FIGURE_TYPE_CIRCLE = 0;
    public static final int FIGURE_TYPE_RECTANGLE = 1;
    public static final int FIGURE_TYPE_TRIANGLE = 2;
    public static final int FIGURE_TYPE_CAR = 3;
    public static final int FIGURE_TYPE_SMILE = 4;

    private int type;

    protected double cX;
    protected double cY;
    protected double lineWidth;
    protected Color color;

    public Figure(int type, double cX, double cY, double lineWidth, Color color) {

        this.type = type;
        this.cX = cX;
        this.cY = cY;
        this.lineWidth = lineWidth;
        this.color = color;
    }

    public double getcX() {
        return cX;
    }

    public void setcX(double cX) {
        this.cX = cX;
    }

    public double getcY() {
        return cY;
    }

    public void setcY(double cY) {
        this.cY = cY;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getType() {
        return type;
    }

}
