package com.stormnet.figuresfx.controller;

import com.stormnet.UnknownFigureTypeException;
import com.stormnet.drawutils.Drawer;
import com.stormnet.figuresfx.figures.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(Logger.class);

    private List<Figure> figures;
    private Random random;
    static int count = 0;

    @FXML
    private Canvas canvas;

    @FXML
    private Button btnSave;

    @FXML
    private Button btnClear;

    @FXML
    private TextField saveStmnt;

    @FXML
    private TextField clearStmnt;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new ArrayList<>();
        random = new Random(System.currentTimeMillis());

    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        try {
            addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        } catch (UnknownFigureTypeException e) {
            LOGGER.info("Unknown figure!");
        }
        repaint();
    }

    public void clear() {

        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        figures = new ArrayList<>();
        clearStmnt.setText("Please, start again!");
    }


    public void save() {

        WritableImage img = new WritableImage(1024, 600);
        canvas.snapshot(null, img);
        count++;
        File f1 = new File("src/com/stormnet/drawutils/screens/image(" + count + ").png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(canvas.snapshot(null, img), null), "png", f1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveStmnt.setText("Image successfully saved");

    }

    private void repaint() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);
        drawer.draw(canvas.getGraphicsContext2D());

        saveStmnt.clear();
        clearStmnt.clear();
    }

    private void addFigure(Figure figure) {
        figures.add(figure);
    }

    private Figure createFigure(double x, double y) throws UnknownFigureTypeException {
        Figure figure = null;

        switch (random.nextInt(6)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(3), Color.RED, random.nextInt(50));
                break;

            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(3), Color.BLUE, random.nextInt(50), random.nextInt(50));
                break;

            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(3), Color.CORAL, random.nextInt(50));
                break;

            case Figure.FIGURE_TYPE_CAR:
                figure = new Car(x, y, random.nextInt(3), Color.BLACK, random.nextInt(50), random.nextInt(50), random.nextInt(50));
                break;

            case Figure.FIGURE_TYPE_SMILE:
                figure = new Smile(x, y, random.nextInt(3), Color.GOLD, random.nextInt(50));
                break;

            default:
                throw new UnknownFigureTypeException();
        }
        return figure;
    }
}
